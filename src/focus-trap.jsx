let FocalPoint = require('./FocalContainer')
let React      = require('react')

let FocusComp = React.createClass({

  propTypes: {
    active : React.PropTypes.bool,
    onExit : React.PropTypes.func
  },

  getDefaultProps() {
    return {
      active    : true,
      className : 'focus-trap'
    }
  },

  render() {
    let { active, className, children, element, onExit } = this.props

    if (!active ) return null

    return (
      <div className={ `${ className }-wrapper` } onKeyUp={ this._onKeyUp }>
        <div aria-hidden="true" className={ `${ className }-backdrop` } onClick={ onExit } />
        <FocalContainer ref='focus' className={ className } element={ element }>
          { children }
        </FocalContainer>
      </div>
    )
  },

  _onKeyUp(e) {
    if (e.key === 'Escape' && 'onExit' in this.props) {
      this.props.onExit()
    }
  }
})

module.exports = FocusTrap
